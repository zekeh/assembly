INCLUDE asmMac.INC
INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
crlf    equ 13, 10, '$'        
prompt   DB 'enter 3 numbers', crlf
N        DW ?
R        DW ?
K        DW ?


        .CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
equation PROC
        _start
        _DisStr prompt
        call getDec
        mov N, ax
        call getDec
        mov R, ax
        call getDec
        mov K, ax
        mov ax, -5
        mov bx, R
        imul bx ;multiplies negative numbers
        add ax, N
        mov bl, 5
        cwd 
        idiv bl
        
        call putDec
        
         _return 0 
        
        
        
equation ENDP
END equation