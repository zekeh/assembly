INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
RandomBits DW 1010101010101010b
        
        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
        bitop2 PROC
        _start
        
        mov ax, RandomBits
        xor ax, 1010100000001010b
        or ax,  1010101111101010b
        or ax,  1010101100101010b
        call putHex
        
        _return 0
        bitop2 ENDP
        END bitop2