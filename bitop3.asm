INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
        bitop3 PROC
        _start
        
        mov al, 'h'
        xor al, 100000b ;changes the value of the 6th bit
        _disChr al ;displays ascii value of the al register
        
        _return 0
        bitop3 ENDP
        END bitop3