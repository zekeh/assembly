  
  INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .DATA
        .CODE
decrypt PROC


    _disStr msg
    _proStr getString
    _disChr 10
    mov bx, 0
    
     sub bh, bh
     mov al, buffer
     mov bx, 0  
     _disStr msgD
     decryptLoop:
        mov cl, buffer+bx
        sub cl, 3 ;loops through the string and subtracts 3 from each character
      
        _disChr cl
        inc bx
        cmp bl, getCount
        jne decryptLoop
        Ret
        decrypt ENDP