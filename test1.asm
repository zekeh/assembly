        INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
crlf      equ 13, 10, '$'
myName    DB 'Zaiah Hammonds', crlf
date      DB 'Today is: ', '$'
day       DB ?
month     DB ?
year      DW ?
slash     DB '/ $'
        


        .CODE
EXTRN GetDec:NEAR, PutDec:NEAR
Test1   PROC
        _start ;starts program
        _DisStr myName ;display my name
        
        mov ah, 2Ah ;calls for DOS system date
        int 21h
        mov day, dl 
        mov month, dh
        mov year, cx
        
        mov al, day
        call putDec
        _DisStr slash
        mov ah, month
        call putDec
        _DisStr slash
        mov ax, year
        call putDec ;displays year
        
        _return 0 ;ends program
        Test1 ENDP
        END Test1