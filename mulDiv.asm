        INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
crlf    equ 13, 10, '$'
endl    DB crlf
A       DW ?
B       DW ?
C       DW ? 
prompt1 DB 'Enter A B and C', crlf  
prompt2 DB 'Enter D and E', crlf
D       DW ?
E       DW ?
     
        .CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
mulDiv  PROC
        _start
        
        
        _DisStr prompt1
        call getDec
        mov A, ax
        call getDec
        mov B, ax
        call getDec
        mov C, ax
        mov ax, A
        add ax, 1
        mov bx, B
        mul bx
        mov bx, c
        cwd
        div bx
        call putDec
        _DisStr endl
        
        _DisStr prompt2
        call getDec
        mov D, ax
        call getDec
        mov E, ax
        mov D, ax
        mov bx, -6
        imul bx
        mov bx, ax
        mov E, cx
        mov dx, 7
        mul dx
        add ax, bx
        call putDec
        _DisStr endl
        
        mov al, 6
        mov bl, 5
        mul bl
        call putDec
        _DisStr endl
        
        mov al, -6
        mov bl, 5
        imul bl
        call putDec
        _DisStr endl
        
        mov ax, 600
        mov bx, 500
        mul bx
        call putDec
        _DisStr endl
        
        mov ax, -600
        mov bx, 500
        imul bx
        call putDec
        _DisStr endl
        
        mov al, 6
        mov bl, 5
        cbw
        div bl
        call putDec
        _DisStr endl
        
        mov al, -6
        mov bl, 5
        cbw
        idiv bl
        call putDec
        _DisStr endl
        
        mov ax, 600
        mov bx, 500
        cwd
        div bx
        call putDec
        _DisStr endl
        
        mov ax, -600
        mov bx, 500
        cwd
        idiv bx
        call putDec
        _DisStr endl
        
        
        _return 0
mulDiv  ENDP
        END mulDiv   