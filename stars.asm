       INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
crlf    equ 13, 10, '$'
star    DB '*', '$'
space   DB ' ', '$'
endl    DB crlf
     
        .CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
stars  PROC
        _start
        ;if dx == even?
        mov cx, 4 ;lines
        mov dx, 0
        lines:
        mov bx, 8 ;stars
            disSpace:
                _DisStr space
                mov dx, 1
            stuff:
               _DisStr star
                dec bx
                mov dx, 0
                jnz stuff
             jmp otherThings 
             
            otherThings:   
            cmp dx, 0
            je disSpace 
            
            _DisStr endl
            dec cx
            mov dx, 0
            jnz lines
        _DisStr endl
        _DisStr endl
        _DisStr endl
        _DisStr endl
        _return 0
        stars  ENDP
        END stars