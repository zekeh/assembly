INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
     
        month DW ?
        
        
        
        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
        shift2 PROC
        _start
        ;year month day      
        ;10001 0011 0110
        mov ax, 0001000100110110b
        shl ax, 8
        shr ax, 12
        mov month, ax
        mov ax, month
        call putHex
        
        
        _return 0
        shift2 ENDP
        END shift2
     