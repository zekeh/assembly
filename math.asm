INCLUDE asmMac.INC
INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA

       crlf equ 13, 10, '$'
       endl dw crlf
        .CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
math PROC
        _start
        mov ax, 0FFFFh
        inc ax
        ;Call putHex
        mov ax, 0ffffh
        inc al
        ;call putHex
        mov ax, 0ffffh
        inc ah
        ;call putHex
        mov ax, 1234h
        sub ax, 35h
        ;call putHex
        mov ax, 1234h
        neg ax
        ;call putHex
        mov ax, 1234h
        neg al
        ;call putHex
        mov ax, 1234h
        mov al, -1
        ;call putHex
        mov ax, 12FFh
        inc al
        ;call putHex
        mov ax, 12FFh
        inc ax
        ;call putHex
        mov ax, 1200h
        dec ax
        call putHex
        mov ax, 1234h
        sub ax, ax
        ;call putHex
        mov ax, 1234h
        sub al, al
        call putHex
         _return 0 
        
        
        
math ENDP
END math