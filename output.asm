        INCLUDE asmMac.INC
                INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
        
crlf    equ 13, 10
Msg1    DB 'using macros is', 13, 10, 'easy$'
Msg2    DB 'The $64,000 question is', 13, 10, '$'
Msg3    DB 'The $' 
Msg8    DB  '64,000 question is', 13, 10, '$'
Msg4    DB "Robert", crlf
Msg5    DB 'R'
Msg6    DB 'o'
Msg7    DB 'b', 13, 10, '$'

        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
Stuff   PROC
      _start

      mov Msg5[0], 'B' ;switches r with b
      mov Msg4[3], ' ' ;removes 'ert'
      mov Msg4[4], ' '
      mov Msg4[5], ' '
        
      _DisStr Msg1
      _DisStr Msg2
      _DisChr '$' 
      _DisStr Msg8
      _DisStr Msg3
      _DisStr Msg4  
      _DisStr Msg5
      lahf
      call putHex
    
      _return 0        
Stuff   ENDP
        END Stuff