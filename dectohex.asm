INCLUDE asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA

msg db 'input a decimal number', 13, 10, '$'

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
dec2hex PROC
    _start
    
    _disStr msg
    call GetDec
    call PutHex
    _return 0
    
    dec2hex ENDP
    END dec2hex