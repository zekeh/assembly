INCLUDE asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA

crlf    equ 13, 10, '$'
prompt   DB 'enter a number', crlf
.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
mac PROC
    _start
    mov dx, OFFSET prompt
    mov ah, 9h
    int 21h
    call GetDec
    call PutHex
    _return 0
    mac ENDP
    END mac