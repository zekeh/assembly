_start MACRO
	mov     ax, @data           
	mov     ds, ax 
	 
	ENDM
_return MACRO returnValue
	mov 	al, returnValue
	mov		ah, 4ch
	int 	21h
	
	ENDM
	
_DisStr MACRO addr
	lea dx, addr
	mov ah, 9h
	int 21h
	ENDM

_DisChr MACRO c1
	mov ah, 02h
	mov dl, c1
	int 21h
	ENDM

_proStr MACRO addr
	lea dx, addr
	mov ah, 0Ah
	int 21h
	ENDM