include asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA


maxChar equ 100
getString DB maxChar
getCount DB ?
buffer DB maxChar DUP(?)

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
reverse PROC ;reverse string
    _start
    
    _proStr getString
    _disChr 10
    
    mov bl, 
    cmp bl, 0
    jz done ;checks to see if count is 0
    
    
    sub bh, bh
    mov di, OFFSET buffer-1
    reverseStr:
        _disChr[bx+di]
        dec bx
        jnz reverseStr
          
    
    done:
    _return 0
    
    reverse ENDP
    END reverse