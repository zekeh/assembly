INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        A DW 0A00h, 1A01h, 2A02h
        B DW 0B00h, 1B01h, 2B02h
        C DB 0Ch, 1Ch, 2Ch, 3Ch, 4Ch, 5Ch
        D DB 0Dh, 1Dh, 2Dh
        BY DB 12h, 34h
        W DW 1234h
        Alpha DB 'ABCDEF'
        E DB 'Hawks', 13, 10, '$'
        F DB 'WIN$'
        
        Endl DB 13, 10, '$'
        
        
        
        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
        ArrPrb PROC
        _start
                    ;page 1
        mov ax, A
        call putHex
        _DisStr Endl
        mov ax, [A]
        call putHex
        _DisStr Endl
        mov ax, [A+4]
        call putHex
        _DisStr Endl
        mov ax, [B+4]
        call putHex
        _DisStr Endl
        mov ax, [B+4]
        call putHex
        _DisStr Endl
        mov ax, [A+10]
        call putHex
        _DisStr Endl
        mov ax, [B-4]
        call putHex
        _DisStr Endl
        mov ax, 0
        mov al, [C+2]
        call putHex
        _DisStr Endl
        mov al, [C+7]
        call putHex
        _DisStr Endl
        mov al, [D]
        call putHex
        _DisStr Endl
        mov al, [D-3]
        call putHex
        _DisStr Endl
                    ;page 2
        mov al, [E + 3]
        call putHex
        _DisStr Endl
        mov al, [E+5]
        call putHex
        _DisStr Endl
        mov al, [F+2]
        call putHex
        _DisStr Endl
        mov al, [E+9]
        call putHex
        _DisStr Endl
        mov al, [F+2]
        call putHex
        _DisStr Endl
                    ;page 3
        mov ax, 1234h
        call putHex
        _DisStr Endl
        mov ax, WORD PTR BY
        call putHex
        _DisStr Endl
        mov ax, W
        call putHex
        _DisStr Endl
        mov ax, 1234h
        call putHex
        _DisStr Endl
        mov ax, 0
        mov al, BYTE PTR W
        call putHex
        _DisStr Endl
        mov ax, WORD PTR Alpha
        call putHex
        _DisStr Endl
        mov ax, WORD PTR Alpha+1
        call putHex
        _DisStr Endl
        
        
        _return 0
        ArrPrb ENDP
        END ArrPrb
     