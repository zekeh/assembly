include asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA

N DW 5
CArray DB 'Intel 80X86'
WArray DW 4, -15, 33, 87, 2, -11
endl DB 13, 10, '$'

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
arrays PROC
    _start
    
    mov bx, 3
    mov al, [CArray+bx]
    _disChr al
    _disStr endl
    
    mov bx, N
    mov al, [CArray+bx]
    _disChr al
    _disStr endl
    mov al, [CArray+2+bx]
    _disChr al
    _disStr endl
    
    mov bx, N
    shl bx, 1
    mov al, [CArray+bx]
    _disChr al
    _disStr endl
    mov al, [CArray-2+bx]
    _disChr al
    _disStr endl
    
    mov bx, OFFSET CArray
    mov al, [bx]
    _disChr al
    _disStr endl
    mov al, [3+bx]
    _disChr al
    _disStr endl
    
    mov bx, OFFSET WArray
    mov al, [bx]
    mov al, [8+bx]
    
    mov bx, OFFSET CArray
    mov si, 3
    mov al, [bx+si]
    _disChr al
    _disStr endl
    mov al, [3+bx+si]
    _disChr al
    _disStr endl
    mov bx, OFFSET WArray
    mov si, 6
    mov ax, [bx+si]
    call putDec
    _disStr endl
    mov ax, [4+bx+si]
    call putDec
    _disStr endl
    
    _return 0
    
    arrays ENDP
    END arrays