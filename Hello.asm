;first program

        .MODEL small
        .STACK 100h
        .DATA

        x       DB 'Hello', '$'
        y       DB 5, 10, 5, 10, 5, 10, 5, 10, 5, 10, 5, 10, '$'
        Letters DB 'ABC', '$'
        Digits  DB 1,2,3, '$'
       ; Numbers DW 6767h, 0ababh, '$'
        More    DB 'e', 10, 'fg', '$'
        Hush    DB 5 DUP('S'), 'Hi!', '$'
        Two3    DB 3 DUP(2, 3, ?), '$'
        Recurse DB 2 DUP('X', 3 DUP(0)), '$'
        Msg1    DB 'All'
                DB 'on one'
                DB 'line', 13, 10, '$'
        Msg2    DB 'One $', 13, 10, 'Two', 13,10, 'Three', 13, 10, '$'
        Msg3    DB 4 DUP(5 DUP('*'), 13, 10), '$'
        Msg4    DB 10 DUP('*'), 13, 10
                DB 3 DUP('*', 8 DUP(' '), '*', 13, 10)
                DB 10 DUP('*'), 13, 10, '$'

        .CODE
Hello   PROC
        mov     ax, @data           
        mov     ds, ax 
        
        
        mov     dx, OFFSET x
        mov     ah, 9h 
        int     21h 
        mov     dx, OFFSET y
        mov     ah, 9h
        int     21h  
        mov     dx, OFFSET Letters
        mov     ah, 9h
        int     21h  
        mov     dx, OFFSET Digits
        mov     ah, 9h
        int     21h  
        mov     dx, OFFSET More
        mov     ah, 9h 
        int     21h 
        mov     dx, OFFSET Hush
        mov     ah, 9h
        int     21h  
        mov     dx, OFFSET Two3
        mov     ah, 9h
        int     21h  
        mov     dx, OFFSET Recurse
        mov     ah, 9h    
        int     21h 
        mov     dx, OFFSET Msg1
        mov     ah, 9h    
        int     21h 
        mov     dx, OFFSET Msg2
        mov     ah, 9h    
        int     21h 
        mov     dx, OFFSET Msg3
        mov     ah, 9h    
        int     21h 
        mov     dx, OFFSET Msg4
        mov     ah, 9h    
        int     21h   
           
        mov     al, 0               
        mov     ah, 4ch             
        int     21h         
Hello   ENDP
        END Hello