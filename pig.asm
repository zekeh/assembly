include asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA


maxChar equ 200
getString DB maxChar
getCount DB ?
buffer DB maxChar DUP(?)
msg DB 'input a string:', 13, 10, '$'
temp DB ?

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
pig PROC
    _start
    
    _disStr msg
    _proStr getString
    _disChr 10
    
    mov bx, 0
    mov di, OFFSET buffer ;moves address of buffer to di reg
    mov cx, 0
    
    ;mov si, buffer + bx
    ;cmp BYTE ptr[si], 'a'
    ;could make cx a pointer and compare to di to move constants
    ;disCh<[di]> if part of a word size pointer
    
    pigLatin:
        inc bx
        cmp BYTE ptr[di+bx], 'a'
        je vowel
        cmp BYTE ptr[di+bx], 'e'
        je vowel
        cmp BYTE ptr[di+bx], 'i'
        je vowel
        cmp BYTE ptr[di+bx], 'o'
        je vowel
        cmp BYTE ptr[di+bx], 'u'
        je vowel
        cmp bl, getCount
        jne pigLatin
        jp done
        vowel:
             mov cx, 1
             mov temp, bx-cx
            _disChr[di+bx] ;puts vowel in front
            cmp BTYE ptr[di+temp], ' '
            jne constanant
            _disChr 'a'
            _disChr 'y'
            _disChr ' '
            jmp pigLatin
            constanant:
                _disChr[di+temp]
                inc bx
                cmp BYTE ptr[di+temp], ' '
                je pigLatin
    done:
    _return 0
    
    pig ENDP
    END pig