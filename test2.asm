include asmMac.INC
include decrypt.asm
include encrypt.asm
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA


maxChar equ 100
getString DB maxChar
getCount DB ?
buffer DB maxChar DUP(?)

msg DB 'input a string:', 13, 10, '$'
msgE DB 'message encrypted: ', 13, 10, '$'
msgD DB 'message decrypted: ', 13, 10, '$'
msgW DB 'which function? 1: encrypt, else: decrypt', 13, 10, '$'

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
main PROC
    _start
    
   _disStr msgW
   call getDec
   cmp ax, 1
   je encrypted ;encrypts if 1, otherwise decrypts
   jmp decrypted
   
   encrypted:
    call encrypt
    jmp done
   decrypted:
    call decrypt
          
    done:
    _return 0
    
    main ENDP
    END main