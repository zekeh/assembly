INCLUDE asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA

crlf     equ 13, 10, '$'
prompt   DB 'enter a Char', crlf
answer   DB 'the conversion result is', crlf
X        DW ?

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
mac PROC
    _start
    _DisStr prompt
    _ReadChar
    mov     X , ax
    _DisStr answer
    _WriteChar X
    _return 0
    mac ENDP
    END mac