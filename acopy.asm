include asmMac.INC
INCLUDELIB UTIL.LIB
.MODEL  small
.STACK  100h
.DATA

W DW 1,2,3,4,5
X DW ?

.CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
acopy PROC
    _start
    mov cx, 5
    mov bx, 0
    copyW:
        mov ax, [W+bx]
        mov [X + bx],ax ;copy values of W to X
        add bx, 2 ;it is a word so its 2 bytes
        dec cx
        jnz copyW
        
    mov si, OFFSET W ;source index: address of W
    mov di, OFFSET X ;destination index: address of X
    copy2:
        mov ax, [si]
        mov [di], ax
        add si, 2
        add di, 2
        dec cx
        jnz copyW
    
    
    _return 0
    
    acopy ENDP
    END acopy