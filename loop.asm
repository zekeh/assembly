       INCLUDE asmMac.INC
       INCLUDE mid.asm
       INCLUDE subLoop.asm
       INCLUDE bot.asm
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
crlf    equ 13, 10, '$'
vert    equ 179
horiz   equ 196
ul      equ 218
ur      equ 191
ll      equ 192
lr      equ 217
endl    DB crlf
l       DW ?
w       DW ?
msg1    DB 'enter a length', crlf
msgw    DB 'enter a width', crlf
     
        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
looq  PROC
        _start
        
        call subLoop
        call mid
        call bot
        
        _return 0
        looq  ENDP
        
        END looq 