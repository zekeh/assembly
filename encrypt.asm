
  INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .DATA
        .CODE
encrypt PROC


    _disStr msg
    _proStr getString
    _disChr 10
    mov bx, 0
    
    sub bh, bh
    mov al, buffer
    _disStr msgE
    encryptLoop:
        mov cl, buffer+bx
        add cl, 3 ;loops through the string and adds 3 to each character
        
        _disChr cl
        inc bx
        cmp bl, getCount
        jne encryptLoop
        Ret
        encrypt ENDP