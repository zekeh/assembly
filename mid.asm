        INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .DATA
        .CODE


mid PROC

mov bx, l
        middle:
            _DisChr vert
            mov cx, w
        
            spaces:
                _DisChr 0
                dec cx
        
                 jnz spaces
                 _DisChr vert
                 _DisStr endl
                dec bx
                jnz middle
           
            
        _DisChr ll
        
        ret
        mid ENDP