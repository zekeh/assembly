        INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
        Endl DB 13, 10, '$'
        
        
        
        .CODE
        EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
        shift1 PROC
        _start
        mov cl, 3
        
        mov ax, 0
        mov al, 11010110b
        shl al, 1
        call putHex
        _disStr Endl
        mov al, 11010110b
        shr al, 1
        call putHex
        _disStr Endl
        mov al, 11010110b
        shl al, cl
        call putHex
        _disStr Endl
        mov al, 11010110b
        sal al, 1
        call putHex
        _disStr Endl
        mov al, 11010110b
        sar al, 1
        call putHex
        _disStr Endl
        mov al, 11010110b
        sal al, cl
        call putHex
        _disStr Endl
        mov al, 11010110b
        sar al, cl
        call putHex
        _disStr Endl
        mov al, 11010110b
        rol al, 1
        call putHex
        _disStr Endl
        mov al, 11010110b
        ror al, 1
        call putHex
        _disStr Endl
        mov al, 11010110b
        rol al, cl
        call putHex
        _disStr Endl
        
        _return 0
        shift1 ENDP
        END shift1
     
      