       INCLUDE asmMac.INC
        INCLUDELIB UTIL.LIB
        .MODEL small
        .STACK 100h
        .DATA
        
crlf    equ 13, 10, '$'
endl    DB crlf
msg1    DB "what is x", crlf
X       DW 10
Y       DW 7
Z       DW ?
A       DW ?
     
        .CODE
EXTRN GetDec:NEAR, PutDec:NEAR, PutHex:NEAR
cmp1  PROC
        _start
        mov x, 10
        mov ax, 5
        cmp x, ax ;compares x to ax
        jg comparey ;if true compare y
        jmp comparez ;if false compare z
      comparey:
        mov y, -1
        mov bx, 0
        cmp y, bx ;compares y to bx
        jl less ;if true a = 10
        jmp compareZ ;if false compare z
        
      compareZ:  
        mov z, 12
        mov cx, 13
        cmp z, cx ;compares z to cx
        jle less
        mov a, 2 ;if z <= 13, a = 10. else: a = 2
        jmp fin
        
        less:
            mov a, 10
            
        fin:
            mov ax, a
            call putDec
          
        _return 0
        cmp1  ENDP
        END cmp1   