Bit: 0 or 1
Byte: 8 bits
Word: 16 bits/2 bytes  (word size varies depeding on processor)
Double word: 2 words
Quad Word:   4 words

Decimal|Binary|Hex
--------------------
0	   |0000b |0000h
1	   |0001b |0001h
2      |0010b |0002h
3      |0011b |0003h
--------------------
4      |0100b |0004h
5      |0101b |0005h
6      |0110b |0006h
7      |0111b |0007h
--------------------
8      |1000b |0008h
9      |1001b |0009h
10     |1010b |000Ah
11     |1011b |000Bh
--------------------
12     |1100b |000Ch
13     |1101b |000Dh
14     |1110b |000Eh
15     |1111b |000Fh
--------------------
16     |10000b|0010h


128	64	32	16	8	4	2	1
---	--	--	--	-	-	-	-



convert decimal to binary:

x = 90   x/2   remainder
         45    0
         22    1
         11    0
         5     1
         2     1
         1     0
         0     1
go from top to bottom:
1011010 = 90

1100111001011b to hexa
1 1001 1100 1011
19CBh


6841 to hex
6841/16 = 427 r 9
427/16 = 26 r 11
26/16 = 1 r 10
1/16 = 0 r 1

1AB9h


 111101011b
  10100110b +
-------------
1010010001b

1EBh
0A6h +
------
291h

leftmost number in binary numbers is the sign bit: 0 positive, 1 is negative
can also use  twos complement: switch 0s and 1s and add 1
change everything from the left of the furthest right 1 and leave everything to the right the same

when adding a negative and its a positive you ignore the overflow
when adding a negative and its a negative you convert it using twos complement 
before representing it as a decimal